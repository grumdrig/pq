ProgressQuest v6.4
==================

BUILDING
--------

Requires Delphi 6.

Requires the package in DelphiZLib.zip to be installed in Delphi, or
decompress the .obj files from there into this, the project, directory.


VCS NOTES
---------

Converted to Mercurial on 1/12/2010 & sent to
http://bitbucket.org/grumdrig/pq/ ; development will continue from
there. If at all. Source opened 5/20/11. The world ends tomorrow anyway.[^1]

This project represents an ex post facto rendering of pq6 in its
various versions in version control, so that changes will be trackable
thataway.

(I didn't add this README file in PQ 6.0, but after that you can use
this README to trace the checkin history using version control.)

Subversion revision numbers vs. versions:

* r321 v6.3 as I found it after a couple of years of inactivity
* r320 v6.2 probably as released, though I'm not totally sure
* r318 v6.2 but probably a slightly older version
* r317 v6.1
* r313 v6.0

(But these revision numbers aren't immediately accessible, if at all,
since moving to git.)


SEE ALSO
--------

The in-browser edition: this project, ported to JavaScript/HTML
https://bitbucket.org/grumdrig/pq-web


[^1]: Although I don't remember it here in 2022, this is presumably a reference
	  to Christian radio host Harold Camping's prediction that the world would
	  end on 5/21/11 at 6pm. It did not, to the dismay of himself and his
      followers (only).
