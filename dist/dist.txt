Reminder: How I publish a new version
-------------------------------------

	$ cd pq/dist
	$ cp ../pq.exe .
	$ zip pq6-MAJOR-MINOR pq.exe README.html license.txt
	$ md5 pq6-MAJOR-MINOR.zip
	$ shasum pq6-MAJOR-MINOR.zip
	$ du -h pq6-MAJOR-MINOR.zip
	$ git add pq6-MAJOR_MINOR.zip
	$ git commit -am "new version!"
	$ scp pq6-MAJOR-MINOR.zip progressquest.com:www/progressquest.com/dl/
	$ ssh progressquest.com
	$ cd www/progressquest.com
	$ nano dl.php
	[copypasta in the new version and checksums and size]
	$ nano releasenotes.php
	[type in the changes]
	$ cd dl
	$ git add pq6-MAJOR-MINOR.zip
	$ git commit -am "New version published!"
	$ did Released PQ version 6-MAJOR-MINOR
